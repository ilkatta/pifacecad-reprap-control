#!/bin/bash
virtualenv_dir=${1-hercules-prusa-control-env}
VIRTUALENV_DIRNAME=`readlink -f "$virtualenv_dir"`

echo "creating virtualenv in $VIRTUALENV_DIRNAME directory"
virtualenv --system-site-packages $VIRTUALENV_DIRNAME

# activate
source $VIRTUALENV_DIRNAME/bin/activate

# create download dir
mkdir -p $VIRTUALENV_DIRNAME/packages

# pyusb
cd $VIRTUALENV_DIRNAME/packages
git clone https://github.com/walac/pyusb.git
cd $VIRTUALENV_DIRNAME/packages/pyusb
python setup.py install

# pifacecad
cd $VIRTUALENV_DIRNAME/packages
git clone https://github.com/piface/pifacecad.git
cd pifacecad
python setup.py install

# cython
pip install --install-option="--prefix=`pwd`" cython

# Printrun
cd $VIRTUALENV_DIRNAME/packages
git clone https://github.com/kliment/Printrun.git
cd $VIRTUALENV_DIRNAME/packages/Printrun
python setup.py build_ext --inplace
python setup.py install

cd $VIRTUALENV_DIRNAME/

# dropbox
pip install dropbox