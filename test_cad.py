__author__ = 'katta'

import pifacecad
from pifacecad.tools.scanf import LCDScanf
import time

'''
scanner = LCDScanf("print: %m.", custom_values=('..','File1.stl', 'File2.stl', 'mega_lungo_file.ciao_ciao_come_va.stl'))
print(scanner.scan()[0])
'''
from pifacecad.tools.question import LCDQuestion
cad = pifacecad.PiFaceCAD()
cad.lcd.backlight_on()
cad.lcd.cursor_off()
cad.lcd.blink_off()
cad.lcd.set_cursor(0, 0)
file_list=['file1.stl','file_with_very_very_long_name.stl','file2.stl']

while True:
    answers=['..',]
    answers.extend(file_list)
    question = LCDQuestion(
        question="file to print:",
        answers=answers,
        selector="->",
        cad=cad
        )
    answer_index = question.ask()
    print(answer_index)
    if answer_index:
        choosen_file=answers[answer_index]
        cad.lcd.clear()
        cad.lcd.set_cursor(0,0)
        cad.lcd.write("File:")
        cad.lcd.set_cursor(0,1)
        cad.lcd.write(choosen_file)
        listener = pifacecad.SwitchEventListener(chip=cad)
        listener.register(5,pifacecad.IODIR_FALLING_EDGE,None)
        listener.register(6,pifacecad.IODIR_FALLING_EDGE,None)
        listener.register(7,pifacecad.IODIR_FALLING_EDGE,None)
        listener.activate()
        while True:
            g=listener.event_queue.get()
            if g.pin_num is 6:
                cad.lcd.move_left()
            if g.pin_num is 7:
                cad.lcd.move_right()
            if g.pin_num is 5:
                break
        listener.deactivate()
        question = LCDQuestion(
            question=choosen_file,
            selector="Are you sure? ",
            answers=['N','Y'],
            cad=cad
        )
        answer = question.ask()
        if answer:
            break
    else:
        file_list=['ciao',]
cad.lcd.clear()
