__author__ = 'katta'

from PiCad import PiCad
import time
from file_manager import FileManager
import os
import dropbox
import config

with PiCad() as pc:

    def write(first_line,second_line=""):
        pc.write(first_line,0)
        print first_line,second_line
        pc.write(second_line,1)

    def getauth():
        config_file = os.path.expanduser('~/.rpi3dprinter')

        if not os.path.exists(config_file):
            flow = dropbox.client.DropboxOAuth2FlowNoRedirect(
                config.DROPBOX['APPKEY'], 
                config.DROPBOX['APPSECRET'],
                )
            authorize_url = flow.start()
            write("please visit url: %s" % authorize_url, "and paste token in console")
            code = raw_input("paste token here: ").strip()
            access_token, user_id = flow.finish(code)
            with open(config_file,'wb') as fd:
                fd.write("%s:%s" % (access_token, user_id) )
            write("Authentication","complete!")
        else:
            with open(config_file,'r') as fd:
                access_token, user_id = fd.read().strip().split(':')
        return access_token, user_id


    access_token, user_id = getauth()

    stl_fm = FileManager(access_token,'stl')
    slic3r_fm = FileManager(access_token,'slic3r')
    if __name__ == '__main__':
        stl_choose=pc.choose('Scegli stl', stl_fm.getFileList(),loop=True)
        write("File choosen %s " % stl_choose)
        pc.write("Slic3r configuration")
        pc.waitInput()
        slic3r_conf = {}

        slic3r_conf['filament'] = pc.choose('Filament setting', slic3r_fm.getFileList('slic3r/filament'),loop=True)
        slic3r_conf['printer'] = pc.choose('Printer setting', slic3r_fm.getFileList('slic3r/printer'),loop=True)
        slic3r_conf['print'] = pc.choose('Print setting', slic3r_fm.getFileList('slic3r/print'),loop=True)

        print(slic3r_conf)