__author__ = 'katta'

from djcontrol import *
import time, sys
from printrun.printcore import printcore



class Leds(object):
    KEY_LEDS = {
        'PLAY_B' : [1,0b10000],
        'PLAY_A' : [1,0b100],
        'FILES' : [1,0b1000],
        'MASTER_A' : [2,0b1],
        'SYNC_B' : [2,0b10000],
        'LISTEN_A' : [0,0b100],
        'LISTEN_B' : [0,0b10000]
    }
    def __init__(self,djc):
        self.djc=djc
        self.curr_state = [0,0,0,0,0]

    def switch(self,key,loc=None):
        if type(key) is str:
            loc,key=self.KEY_LEDS[key]
        c = self.curr_state[loc] ^ key
        self.djc.SetButtons(loc,c)
        self.curr_state[loc] = c

    def state(self,key,loc=None):
        if type(key) is str:
            loc,key=self.KEY_LEDS[key]
        return self.curr_state[loc] & key


class MyPrinter(object):
    MOVE_X_REL = 55  # piatto sx
    MOVE_Y_REL = 57  # piatto dx
    MOVE_X = 54  # cross
    MOVE_Y = 49 # dx
    MOVE_Z = 53 # sx
    TEMP_E1 = 56
    ENABLE_X_REL = 17
    ENABLE_Y_REL = 37


    def __init__(self, port, baud, max_x=200, max_y=200, max_z=150, max_te=300, djc=None):
        self.mx = max_x
        self.my = max_y
        self.mz = max_z
        self.mte = max_te
        self.pc = printcore(port, baud)
        self.port = port
        self.baud = baud
        self.leds = Leds(djc)


    def exec_cmd(self, cmd, value, prev_value=None):
        cmd = int(cmd)
        value = int(value)

        def normalize_value(val):
            if val > 100:
                return val - 255
            if val < -100:
                return val + 255
            return val

        if cmd is self.MOVE_Y_REL:
             if self.leds.state('PLAY_B'):
                y = normalize_value(value - prev_value)
                self.move_y(y, False)

        if cmd is self.MOVE_X_REL:
            if self.leds.state('PLAY_A'):
                x = normalize_value(value - prev_value)
                self.move_x(x, False)

        if cmd is self.MOVE_Z:
            z = value * self.mz / 255
            self.move_z(z)

        if cmd is self.MOVE_X:
            x = value * self.mx / 255
            self.move_x(x)

        if cmd is self.MOVE_Y:
            y = value * self.my / 255
            self.move_y(y)

        if cmd is self.TEMP_E1:
            e = normalize_value(value - prev_value)
            self.temp_e1(e, False)

        if cmd is self.ENABLE_X_REL and value is 1:
            self.leds.switch('PLAY_A')

        if cmd is self.ENABLE_Y_REL and value is 1:
            self.leds.switch('PLAY_B')

        if cmd is 58:
            self.djc.SetButtons(1,hex(int(value)))


    def move_y(self, y, abs_v=True):
        print( "move_y( %d, %s )" % (y, abs_v) )

    def move_x(self, x, abs_v=True):
        print( "move_x( %d, %s )" % (x, abs_v) )

    def move_z(self, z, abs_v=True):
        print( "move_z( %d, %s )" % (z, abs_v) )

    def temp_e1(self, e, abs_v=True):
        print( "temp_e1( %d, %s ) " % ( e, abs_v ) )

    def abs_x(self):
        return self.pc.analyzer.abs_x

    def abs_y(self):
        return self.pc.analyzer.abs_y

    def abs_z(self):
        return self.pc.analyzer.abs_z

    def close(self):
        self.pc.disconnect()


if __name__ == '__main__':
    try:
        djc = djcontrol()
        con = controls()
        con.loadConfig("conf/hercules_djcontrol_mp3_le.conf")
        djc.ClearButtons()

        printer = MyPrinter(sys.argv[1], sys.argv[2],djc=djc)

        con.readControls(djc.GetButtons())
        ch = con.listChanges()

        while True:
            con.readControls(djc.GetButtons())
            ch = con.listChanges()
            for cc in ch:
                print("%d : %d %d  " % ( cc, con.curr_values[cc], con.prev_values[cc] ) )
                printer.exec_cmd(cc, con.curr_values[cc], con.prev_values[cc])
            time.sleep(0.1)
    except:
        try:
            printer.close()
        except:
            pass
        raise
