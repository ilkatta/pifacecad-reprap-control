__author__ = 'katta'

import pifacecad
from file_manager import *
import dropbox
import config
import time
from multiprocessing import Manager,Event,Process,Lock
from pifacecad.tools.question import LCDQuestion
import uuid

class PiCad(object):
    DISPLAY_DIM = pifacecad.lcd.LCD_WIDTH
    '''
    @property
    def lcd(self):
        if not hasattr(self, '_lock'):
            print("************* no _lock attribute **********************")
            self._lock = Lock()
        with self._lock:
            if not hasattr(self, 'picad'):
                self.picad = pifacecad.PiFaceCAD()
                self.picad.lcd.backlight_on()
                self.picad.lcd.cursor_off()
                self.picad.lcd.blink_off()
                self.picad.lcd.set_cursor(0, 0)
                self.picad.lcd.displaymode = pifacecad.lcd.LCD_ENTRYLEFT | pifacecad.lcd.LCD_ENTRYSHIFTDECREMENT
                self.picad.lcd.update_entry_mode()
            return self.picad.lcd
    '''
    @property
    def screen(self):
        with self._lock_screen:
            if not hasattr(self, '_screen_ns'):
                self._set_screen_ns()
            return [ str(x) for x in self._screen_ns.value ]

    @screen.setter
    def screen(self,value):
        with self._lock_screen:
            if not hasattr(self, '_screen_ns'):
                self._set_screen_ns()
            self._screen_ns.value = value
            self._event.set()
    @property
    def run_autoscroll(self):
        if hasattr(self._screen_ns, 'autoscroll'):
            return bool(self._screen_ns.autoscroll)
        else:
            return False

    @run_autoscroll.setter
    def run_autoscroll(self,value):
        try:
            v = bool(value)
            self._screen_ns.autoscroll = v
            self._event.set()
        except:
            pass

    def __init__(self):
        self._lock = Lock()
        self._lock_screen = Lock()
        self.run_autoscroll = True
        with self._lock:
            self.picad = pifacecad.PiFaceCAD()
            self.picad.lcd.backlight_on()
            self.picad.lcd.cursor_off()
            self.picad.lcd.blink_off()
            self.picad.lcd.set_cursor(0, 0)
            self.picad.lcd.displaymode = pifacecad.lcd.LCD_ENTRYLEFT | pifacecad.lcd.LCD_ENTRYSHIFTDECREMENT
            self.picad.lcd.update_entry_mode()
            self.lcd = self.picad.lcd
        self.clear()
        self.autoscroll_worker = Process(target=self.autoscroll)
        self.autoscroll_worker.start()

    def write(self,text,line=0):
        #print("[write] writing '%s' on line %d" % (text,line) )
        lines = self.screen
        lines[line] = text 
        self.screen = lines
        #print('[write] screen[line] > %s' % self.screen)
        
    def _write(self,texts=None):
        if not texts:
            texts = self.screen
        with self._lock:
            self.lcd.set_cursor(0,0)
            self.lcd.write(texts[0])
            self.lcd.set_cursor(0,1)
            self.lcd.write(texts[1])

    def autoscroll(self):
        t = 1
        while True:
            self._event.wait()
            self._event.clear()
            #print("[autoscroll] *** event catched ***")
            #print("[autoscroll] clearing screen")
            self.clear_lcd()
            #print("[autoscroll] Values %s" % self._screen_ns.value)
            screen_val = self.screen
            lens = [
                len(self.screen[0]),
                len(self.screen[1])
            ]
            while not self._event.is_set():
                self._pos = 0
                if max(lens) < self.DISPLAY_DIM :
                    self._write()
                    time.sleep(t)
                elif not self.run_autoscroll:
                    self._write()
                    time.sleep(t)
                else:
                    while self._pos < ( max(lens)-self.DISPLAY_DIM ) :
                        if self._event.is_set():
                            break
                        self._scroll()
                        self._pos += 1
                        time.sleep(t)
                    while self._pos > 0 :
                        if self._event.is_set():
                            break
                        self._scroll
                        self._pos -= 1
                        time.sleep(t)
    
    def _scroll(self):
        screen_val = self.screen
        for line in range(2):
            if self._pos <= (len(self.screen[line])-self.DISPLAY_DIM):
                screen_val[line] = self.screen[line][self._pos:16+self._pos]
            #print("[autoscroll] line %d : %s > %s" % ( line,self.screen[line],screen_val[line]) )
        self._write(screen_val)


    def clear(self):
        self.clear_lcd()
        self.screen = ['','',]

    def clear_lcd(self):
        with self._lock:
            self.lcd.clear()
            self.lcd.home()

    def _set_screen_ns(self):
        if not hasattr(self, 'mgr'):
            self.mgr = Manager()
            self._screen_ns = self.mgr.Namespace()
            self._screen_ns.autoscroll = True
            self._screen_ns.value = []
        if not hasattr(self, '_event'):
            self._event = Event()
        if not hasattr(self, '_lock'):
            self._lock = Lock()

    def waitInput(self,keys=None):
        if not keys:
            keys = range(8)
        listener = pifacecad.SwitchEventListener(chip=self.picad)
        for key in keys:
            listener.register(key,pifacecad.IODIR_FALLING_EDGE,None)
        listener.activate()
        r = listener.event_queue.get()
        while r.pin_num not in keys:
           r = listener.event_queue.get() 
        listener.deactivate()
        return r

    def choose(self,text,qlist,confirm=False,loop=False,back_val=None):
        if loop:
            r = self.choose(text, qlist,confirm=True)
            while not r:
                r = self.choose(text, qlist,confirm=True)
            return r
        back_val = back_val or uuid.uuid4().int
        back = {'..':back_val,}
        qkeys = qlist.keys()
        question = LCDQuestion(
            question=text,
            answers=qkeys,
            selector="",
            cad=self.picad
        )
        self.autoscroll = False
        with self._lock:
            answer = question.ask()
        if type(qlist[qkeys[answer]]) == dict:
            qlist_rec = qlist[qkeys[answer]]
            qlist_rec.update(back)
            r = self.choose(text, qlist_rec,confirm=confirm,back_val=back_val)
            if r == back_val:
                return self.choose(text, qlist,confirm=confirm,back_val=back_val)
            return r
        if confirm and qlist[qkeys[answer]] != back_val:
            self.autoscroll = True
            self.write('press a key',1)
            self.write(qkeys[answer],0)
            self.waitInput()
            self.clear()
            conf_question = LCDQuestion(
                question=qkeys[answer],
                selector='Confirm ? ',
                answers=['No','Yes'],
                cad=self.picad
            )
            with self._lock:
                conf_answer = conf_question.ask()
            if not conf_answer:
                self.clear()
                return None
        self.clear()
        return qlist[qkeys[answer]]

    def exit(self):
        self.autoscroll_worker.terminate()

    def __del__(self):
        self.exit()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.exit()