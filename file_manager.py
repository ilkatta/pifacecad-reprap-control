__author__ = 'katta'
import dropbox
from collections import defaultdict
from os.path import basename

class DFile(object):
    readed = False
    def __init__(self,path,cli):
        self.name = basename(path)
        self._data = None
        self._metadata = None
        self.path = path
        self.client = cli

    def download_data(self):
        self._data, self._metadata = self.client.get_file_and_metadata(self.path)
        
    @property
    def data(self):
        if not self._data:
            self.download_data()
        return self._data
    @data.deleter
    def data(self):
        pass
    @data.setter
    def data(self,value):
        pass

    @property
    def metadata(self):
        if not self._metadata:
            self.download_data()
        return self._metadata
    @metadata.setter
    def metadata(self,value):
        pass
    @metadata.deleter
    def metadata(self):
        pass
    
    def read(self):
        if not self.data or self.readed:
            self.download_data()
        self.readed = True
        return self.data.read()
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.__str__()
    def __repr__(self):
        return '<%s of %s>' % (type(self).__name__, self.name )

class DDir(object):
    def __init__(self,name):
        if name == '':
            name = '/'
        self.name = name
        self.list = []
    def __str__(self):
        return "%s: %s" % ( self.name, str(self.list) )
    def __unicode__(self):
        return "%s: %s" % ( self.name, str(self.list) )
    def append(self,e):
        self.list.append(e)
    def extend(self,l):
        self.list.extend(l)
    def __len__(self):
        return len(self.list)
    def __getitem__(self,key):
        #return self.list.__getitem__(key)
        try:
            return type(self.list).__getitem__(self.list, key)
        except:
            return [ x for x in self.list if x.name == key ][0]

    def __setitem__(self,k,v):
        self.list[k] = v
    def len(self):
        return self.__len__()
    def __repr__(self):
        return '<%s of %s>' % (type(self).__name__, self.name )

class FileManager(object):
    def __init__(self,access_token,root='/'):
        self.client = dropbox.client.DropboxClient(access_token)
        self._fileList = None
        self._root = root
    
    @property
    def fileList(self):
        if not self._fileList:
            self._fileList = self._getFileList()
        return self._fileList

    @property
    def name(self):
        return self.__name__

    @property
    def __name__(self):
        return self._root

    def _getFileList(self,path=None,update=False):
        ''' return the first level of files tree as list type '''
        if not path:
            path = self._root
        if self._fileList :
                if not update:
                    return self.fileList[path]

        fileList = DDir(basename(path))
        contents = self.client.metadata(path)['contents']
        for content in contents:
            path = content['path']
            name = basename(path)
            if content['is_dir']:
                fileList.append (self._getFileList(path) )
            else:
                fileList.append( DFile(path,self.client) )
        if path == self._root:
            self._fileList = fileList
        return fileList

    def getFileList(self,fl=None):
        ''' return the entire tree of file as dict, 
            with filename as key and DFile obj as value '''
        if type(fl) == str:
            fl = self._getFileList(fl)
        fl = fl or self._getFileList()
        fileList = {}
        for e in fl:
            if FileManager.isDir(e):
                fileList.update({ e.name: self.getFileList(e), })
            else:
                fileList.update({e.name:e,})
        return fileList

    def update(self):
        ''' force update of file list '''
        self._getFileList(update=True)

    @staticmethod
    def isFile(e):
        return isinstance(e,DFile)

    @staticmethod
    def isDir(e):
        return isinstance(e,DDir)

    
    def __repr__(self):
        return '<%s %s>' % (type(self).__name__, self.name )

    def __str__(self):
        return self.str()

    def str(self,fl=None,spaces="",prev=""):
        ''' recursive method used by __str__ '''
        if not fl:
            fl = self._getFileList()
        for e in fl:
            if FileManager.isDir(e):
                prev+="%s%s/\n" % (spaces,e.name)
                prev+=self.str(e,spaces+"    ")
            if FileManager.isFile(e):
                prev+="%s%s\n" % (spaces,e.name)
        return prev

    def __getitem__(self,key):
        return self.fileList.__getitem__(key)
